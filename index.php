<?php
error_reporting(E_ALL ^ (E_WARNING | E_NOTICE));
session_start();
if(!isset($_SESSION['email'])) {
   header('location:login.php'); 
} else { 
   $email = $_SESSION['email']; 
}

$koneksi = new mysqli("localhost","root","","sepatu");
?>
<!DOCTYPE html>
<html>
<head>
	<title>Toko Sepatu</title>
	<link rel="stylesheet" href="admin/assets/css/bootstrap.css">
</head>
<body>
<nav class = "navbar navbar-default">
	<div class="container">

	<ul class = "nav navbar-nav">
		<li><a href="home.php">Home</a></li>
		<li><a href="keranjang.php">Keranjang</a></li>
		<li><a href="login.php">Login</a></li>
		<li><a href="checkout.php">Checkout</a></li>
		<li><a href="logout.php">Logout</a></li>
	</ul>
</nav>
<section class="konten">
<div class="container">
	<h1>Produk terbaru</h1>

	<div class="row">
		
		<?php $ambil=$koneksi->query ("SELECT * FROM produk"); ?>
		<?php while($perproduk=$ambil->fetch_assoc()) { ?>
		<div class="col-md-3">
		<div class="thumbnail">
		<img src="foto_produk/<?php echo $perproduk['foto_produk']; ?>" alt=""> 
		<div class="caption">
			<h3><?php echo $perproduk['nama_produk']; ?></h3>
			<h5>Rp. <?php echo number_format( $perproduk['harga_produk']); ?></h5>
			<a href="beli.php?id=<?php echo $perproduk['id_produk']; ?>" class="btn btn-primary">Beli</a>
		</div>
		</div>
	</div>
		<?php } ?>
	
	</div>
</div>
</div>


</body>
</html>